#!/usr/bin/env python
import os
import sys

sys.path.insert(0, os.path.abspath("."))
from tuhls.core.manage import run_example  # noqa

run_example()
