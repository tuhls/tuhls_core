# Generated by Django 3.1.8 on 2021-05-18 22:29

import uuid

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Request',
            fields=[
                ('id', models.BigAutoField(editable=False, primary_key=True, serialize=False)),
                ('gid', models.UUIDField(default=uuid.uuid4, editable=False, unique=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('user', models.UUIDField()),
                ('method', models.CharField(max_length=8)),
                ('status', models.IntegerField()),
                ('ip', models.CharField(max_length=16)),
                ('request_data', models.CharField(max_length=8192)),
                ('response_data', models.CharField(max_length=8192)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
